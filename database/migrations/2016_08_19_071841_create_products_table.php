<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_groups_id');
            $table->string('name');
            $table->text('description');
            $table->text('short_description');
            $table->string('asin');
            $table->string('sku');
            $table->string('url');
            $table->string('price');
            $table->text('image_path');
            $table->text('image_thumb');
            $table->integer('quantity');
            $table->string('is_enable')->default('yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
