@include('admin.include.header')
@include('admin.include.head')

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    @yield('title')
    @yield('favicon')

    @yield('head_script')

</head>

<body class="fixed-left">

<!-- Begin page -->




    @yield('content')




@yield('bottom_script')

</body>
</html>