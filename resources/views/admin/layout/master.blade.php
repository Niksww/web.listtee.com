@include('admin.include.header')
@include('admin.include.head')
@include('admin.include.sidebar')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    @yield('title')
    @yield('favicon')

    <title>Flacto - Responsive Admin Dashboard Template</title>

    @yield('head_script')

</head>

<body class="fixed-left-void" >

<!-- Begin page -->
<div id="wrapper" class="forced enlarged">
    @yield('topbar')
    @yield('left_sidebar')

    @yield('content')

    @yield('right_sidebar')

</div>
<!-- END wrapper -->

@yield('bottom_script')

<script type="text/javascript">
    $(document).ready(function() {
        $('form').parsley();
    });
</script>

</body>
</html>