@section('favicon')
    <link rel="shortcut icon" href="{{ URL('assets/admin/images/favicon.png') }}">
@endsection
@section('head_script')
{{ Html::style('/assets/admin/css/bootstrap.min.css') }}

{{ Html::style('/assets/admin/css/menu.css') }}

{{ Html::style('/assets/admin/css/core.css') }}

{{ Html::style('/assets/admin/css/components.css') }}

{{ Html::style('/assets/admin/css/icons.css') }}

{{ Html::style('/assets/admin/css/pages.css') }}

{{ Html::style('/assets/admin/css/responsive.css') }}

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

{{ Html::script('/assets/admin/js/modernizr.min.js') }}
@endsection

@section('bottom_script')
<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
{{ Html::script('/assets/admin/js/jquery.min.js') }}
{{ Html::script('/assets/admin/js/bootstrap.min.js') }}
{{ Html::script('/assets/admin/js/detect.js') }}
{{ Html::script('/assets/admin/js/fastclick.js') }}
{{ Html::script('/assets/admin/js/jquery.slimscroll.js') }}
{{ Html::script('/assets/admin/js/jquery.blockUI.js') }}
{{ Html::script('/assets/admin/js/waves.js') }}
{{ Html::script('/assets/admin/js/wow.min.js') }}
{{ Html::script('/assets/admin/js/jquery.nicescroll.js') }}
{{ Html::script('/assets/admin/js/jquery.scrollTo.min.js') }}

        <!-- Validation js (Parsleyjs) -->
{{ Html::script('/assets/admin/plugins/parsleyjs/dist/parsley.min.js') }}

        <!-- App js -->
{{ Html::script('/assets/admin/js/jquery.core.js') }}
{{ Html::script('/assets/admin/js/jquery.app.js') }}
@endsection