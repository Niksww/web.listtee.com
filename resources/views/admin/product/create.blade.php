@extends('admin.layout.master')
@section('title')
    <title>Add Product - Listtee | Web application</title>
@endsection
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12" >

                        <div class="col-lg-12">
                            <div class="btn-group pull-right m-t-5 m-b-20">
                                <a href="{{ url('/admin/product/') }}" class="btn btn-custom" >View all Products <span class="m-l-5"><i class="fa fa-cog"></i></span></a>
                                <a href="{{ URL::previous() }}"  class="btn btn-primary waves-effect m-l-10" > Back</a>
                            </div>
                            <div class="col-lg-6">
                                <h4 class="page-title">Add New Product</h4>
                            </div>

                        </div>

                    </div>
                </div>
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-sm-6" style="margin-left: 10px; margin-top: 20px;">
                        <div class="col-lg-12">
                            <form class="form-horizontal m-t-10" role="form" method="POST" action="{{ url('/admin/product') }}">
                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">Name of item*</label>
                                    <input type="text" name="name" parsley-trigger="change" required
                                           placeholder="Enter name" class="form-control" id="name" value="{{ old('name') }}" />
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="select">
                                        <label for="product_groups_id">Group*</label>
                                        <select name="product_groups_id" id="is_enable" class="form-control" required>
                                            <option value="">Select Group</option>
                                           @foreach($groups as $group)
                                               <option value="{{ $group->id }}"> {{ $group->name }} </option>
                                           @endforeach

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="asin">ASIN*</label>
                                    <input id="asin" type="text" placeholder="ASIN" required name="asin" class="form-control">
                                </div>

                               {{-- <div class="form-group">
                                    <label for="sku">SKU*</label>
                                    <input id="sku" type="text" placeholder="SKU" required name="sku" class="form-control">
                                </div>--}}

                                <div class="form-group">
                                    <label for="price">Cost Price*</label>
                                    <input id="price" type="text" placeholder="Price" required name="price" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="url">Ebay Link*</label>
                                    <input id="url" type="text" placeholder="Ebay Link" required name="url" class="form-control"  />
                                </div>
                               {{-- <div class="form-group">
                                    <div class="select">
                                        <label for="is_enable"> Enable </label>
                                        <select name="is_enable" id="is_enable" class="form-control">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>--}}

                                <div class="form-group text-left m-b-0">
                                    <?php
                                    if (Auth::check())
                                    {
                                        $id = Auth::user()->getId();
                                    }

                                    ?>
                                    <input id="url" type="hidden" name="user_id" value="{{ $id}}"  class="form-control"  />
                                    <input id="url" type="hidden" name="created_by" value="user" class="form-control"  />
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        Save
                                    </button>
                                    <button type="reset" class="btn btn-link waves-effect m-l-5">
                                        Cancel
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- end row -->



            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">

        </footer>

    </div>
@endsection