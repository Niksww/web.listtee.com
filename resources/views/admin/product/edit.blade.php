@extends('admin.layout.master')
@section('title')
    <title>Update Product - Listtee | Web application</title>
@endsection
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        {{-- <div class="btn-group pull-right m-t-5 m-b-20">
                             <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Add Group <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                         </div>--}}
                        <div class="btn-group pull-right m-t-5 m-b-20">
                            <a href="{{ url('/admin/product/') }}" class="btn btn-custom" >View all Products <span class="m-l-5"><i class="fa fa-cog"></i></span></a>
                            <a href="{{ URL::previous() }}"  class="btn btn-primary waves-effect m-l-10" > Back</a>
                        </div>
                        <div class="col-lg-6">

                            <h4 class="page-title">{{ $product->name }}</h4>
                        </div>

                    </div>
                </div>
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-sm-6" style="margin-left: 10px; margin-top: 20px;">
                        <div class="col-lg-12">
                            {{ Form::model($product, array('route' => array('admin.product.update', $product->id), 'method' => 'PUT')) }}


                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">Name of item*</label>
                                    <input type="text" name="name" parsley-trigger="change" required
                                           placeholder="Enter name" class="form-control" id="name" value="{{ $product->name }}" />
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="select">
                                        <label for="product_groups_id">Group*</label>
                                        <select name="product_groups_id" id="is_enable" class="form-control">
                                            @foreach($groups as $group)
                                                <option value="{{ $group->id }}" {{ ($product->product_groups_id == $group->id)?'selected':null }}> {{ $group->name }} </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                              {{--  <div class="form-group">
                                    <label for="description">Description*</label>
                                    <input type="text" name="description" parsley-trigger="change" required
                                           placeholder="Enter description" class="form-control" id="description" value="{{ $product->description }}" />
                                </div>

                                <div class="form-group">
                                    <label for="description">Short Description*</label>
                                    <input type="text" name="short_description" parsley-trigger="change" required
                                           placeholder="Enter short description" class="form-control" id="short_description" value="{{ $product->short_description }}">
                                </div>--}}
                                <div class="form-group">
                                    <label for="asin">ASIN*</label>
                                    <input id="asin" type="text" placeholder="ASIN" required name="asin" class="form-control" value="{{ $product->asin }}" />
                                </div>

                             {{--   <div class="form-group">
                                    <label for="sku">SKU*</label>
                                    <input id="sku" type="text" placeholder="SKU" required name="sku" class="form-control" value="{{ $product->sku }}"/>
                                </div>--}}

                                <div class="form-group">
                                    <label for="price">Cost Price*</label>
                                    <input id="price" type="text" placeholder="Price" required name="price" class="form-control" value="{{ $product->price }}" />
                                </div>

                            <div class="form-group">
                                <label for="url">Ebay Link*</label>
                                <input id="url" type="text" placeholder="Ebay Link" required name="url" class="form-control" value="{{ $product->url }}" />
                            </div>
                            {{--    <div class="form-group">
                                    <label for="quantity">Quantity*</label>
                                    <input id="quantity" type="text" placeholder="Quantity" required name="quantity" class="form-control" value="{{ $product->quantity }}" />
                                </div>
--}}
                                {{--<div class="form-group">
                                    <div class="select">
                                        <label for="is_enable"> Enable </label>
                                        <select name="is_enable" id="is_enable" class="form-control">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>--}}

                                <div class="form-group text-left m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-link waves-effect m-l-5">
                                        Cancel
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- end row -->



            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">

        </footer>

    </div>
@endsection