@extends('admin.layout.master')
@section('title')
    <title>Products - Listtee | Web application</title>
@endsection
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-5 m-b-20">
                            <a href="{{ url('/admin/product/create') }}" class="btn btn-custom" >Add Product <span class="m-l-5"><i class="fa fa-cog"></i></span></a>
                            <a href="#" class="btn btn-success waves-effect waves-light m-l-5 m-r-5" data-toggle="modal" data-target="#import_modal" >Import Products</a>
                            <a href="{{ url('assets/product/products-sample.csv') }}" class="btn btn-success waves-effect waves-light">Download Sample</a>
                            {{--<div class="btn-group">
                                <button class="btn btn-info">Export</button>
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu" id="export-menu">
                                    <li id="export_to_excel"> <a href="#">Export To Excel</a> </li>
                                    <li class="divider"></li>
                                    <li> <a href="#">Other</a> </li>
                                </ul>
                            </div>--}}

                        </div>
                        <div class="col-lg-12" style="width:120px;">
                            <h4 class="page-title">Products</h4>
                        </div>

                    </div>
                </div>
                <div id="import_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="postImport" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Import Products</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="file" name="products" />
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-info waves-effect waves-light" >Import</button>

                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                <div class="row">

                    <div class="col-lg-12">
                        @if ($products->count())
                            <table id="datatable" class="table table-striped dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>SR#</th>
                                    <th>Name of item</th>
                                    <th>ASIN</th>
                                    <th>Cost Price</th>
                                    <th>Ebay Link</th>
                                    <th class="text-center">Action</th>

                                </tr>
                                </thead>

                                <tbody>
                                <?php $i=1; ?>
                                @foreach($products as $product)
                                    <tr>
                                        <td><?php echo $i;?></td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->asin }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->url }}</td>
                                        <td class="text-center">
                                            <div class="col-lg-12 text-right">
                                                <div class="col-lg-4 pull-right text-left">
                                                    {!! Form::open([
                                                        'method' => 'DELETE',
                                                        'route' => ['admin.product.destroy', $product->id]
                                                    ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                                <div class="col-lg-2 pull-right">
                                                    <a href="{{ url('/admin/product/'.$product->id) }}" class="btn btn-primary">View</a>
                                                </div>
                                                <div class="col-lg-2 pull-right">
                                                    <a href="{{ url('/admin/product/'.$product->id.'/edit') }}" class="btn btn-primary">Edit</a>
                                                </div>


                                            </div>
                                            </td>

                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            There are no product
                        @endif
                    </div>

                </div>
                <!-- end row -->



            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">

        </footer>

    </div>
@endsection