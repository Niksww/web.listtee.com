@extends('admin.layout.master')
@section('title')
    <title>Groups - Listtee | Web application</title>
@endsection
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-5 m-b-20">
                            <a href="{{ url('/admin/groups/create') }}" class="btn btn-custom" >Add Group <span class="m-l-5"><i class="fa fa-cog"></i></span></a>
                        </div>
                        <div class="col-lg-12" style="width:120px;">
                            <h4 class="page-title">Groups</h4>
                        </div>

                    </div>
                </div>
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                <div class="row">

                    <div class="col-lg-12">
                        @if ($groups->count())
                            <table id="datatable" class="table table-striped dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>SR#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Enable</th>
                                    <th class="text-center">Action</th>

                                </tr>
                                </thead>

                                <tbody>
                                <?php $i=1; ?>
                                @foreach($groups as $group)
                                    <tr>
                                        <td><?php echo $i;?></td>
                                        <td>{{ $group->name }}</td>
                                        <td>{{ $group->description }}</td>
                                        <td>{{ $group->is_enable }}</td>
                                        <td class="text-center">
                                            <div class="col-lg-12 text-right">
                                                <div class="col-lg-4 pull-right text-left">
                                                    {!! Form::open([
                                                        'method' => 'DELETE',
                                                        'route' => ['admin.groups.destroy', $group->id]
                                                    ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                                <div class="col-lg-2 pull-right">
                                                    <a href="{{ url('/admin/groups/'.$group->id) }}" class="btn btn-primary">View</a>
                                                </div>
                                                <div class="col-lg-2 pull-right">
                                                    <a href="{{ url('/admin/groups/'.$group->id.'/edit') }}" class="btn btn-primary">Edit</a>
                                                </div>


                                            </div>
                                            </td>

                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $groups->links() }}
                        @else
                            There are no Group
                        @endif
                    </div>

                </div>
                <!-- end row -->



            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">

        </footer>

    </div>
@endsection