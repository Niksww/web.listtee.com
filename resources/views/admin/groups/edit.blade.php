@extends('admin.layout.master')
@section('title')
    <title>Update {{ $group->name }} Group - Listtee | Web application</title>
@endsection
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-5 m-b-20">
                            <a href="{{ url('/admin/groups/') }}" class="btn btn-custom" >View all Groups <span class="m-l-5"><i class="fa fa-cog"></i></span></a>
                            <a href="{{ URL::previous() }}"  class="btn btn-primary waves-effect m-l-10" > Back</a>
                        </div>
                        <div class="col-lg-6">
                            <h4 class="page-title">Product Group: {{ $group->name }}</h4>
                        </div>

                    </div>
                </div>
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-sm-6">
                        <div class="col-lg-12">
                            {{ Form::model($group, array('route' => array('admin.groups.update', $group->id), 'method' => 'PUT')) }}
                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">Name*</label>
                                    <input type="text" name="name" parsley-trigger="change" required
                                           placeholder="Enter name" class="form-control" id="name" value="{{ $group->name }}" />
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="description">Description*</label>
                                    <input type="text" name="description" parsley-trigger="change" required
                                           placeholder="Enter description" class="form-control" id="description" value="{{ $group->description }}">
                                </div>
                                <div class="form-group">
                                    <label for="url_key">URL Key*</label>
                                    <input id="url_key" type="text" placeholder="Url Key" required name="url_key"
                                           class="form-control"  value="{{ $group->url_key }}"/>
                                </div>

                                <div class="form-group">
                                    <div class="select">
                                        <label for="is_enable"> Enable </label>
                                        <select name="is_enable" id="is_enable" class="form-control">
                                            <option value="Yes" {{ ($group->is_enable == 'Yes')?'selected':null }}>Yes</option>
                                            <option value="No" {{ ($group->is_enable == 'No')?'selected':null }}>No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group text-left m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-link waves-effect m-l-5">
                                        Cancel
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- end row -->



            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">

        </footer>

    </div>
@endsection