@extends('admin.layout.master')

@section('title')
    <title> Dashboard - Listtee | Web application </title>
@endsection
@section('content')
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>

        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer">

    </footer>

</div>
@endsection