@extends('admin.layout.master')
@section('title')
    <title>Add Item - Listtee | Web application</title>
@endsection
@section('content')
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right m-t-5 m-b-20">
                        <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
<<<<<<< HEAD
                    <div class="col-lg-2" style="width:120px;">
                        <h4 class="page-title">Add Item</h4>
                    </div>
                    <div class="col-lg-8">
                        <div class="col-lg-3">
                            <select class="form-control">
                                <option>Price &amp; Grade</option>
                                <option>Grade</option>
                                <option>Presort &amp; Price</option>
                                <option>Presort</option>
                            </select>
                        </div>
                        <div class="col-lg-8">
                            <div class="input-group bootstrap-touchspin">
                                <span class="input-group-btn" style="text-align: right;">
                                    <img src="{{ URL('assets/admin/images/amazon.png')}}" width="37px" />
                                </span>
                                <span>
                                    <input type="text" class="form-control" placeholder="Search item by UPC, EAN, ISBN, ASIN, FNSKU" style="border-top: none; border-bottom: none;" />
                                </span>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="padding: 4px 9px;">
                                        <i class="zmdi zmdi-search" style="color:#2196f3; font-size: 29px;"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
=======
                    <h4 class="page-title">Add Item</h4>
>>>>>>> 05deb3a9c5e736f16e29227a8ea227401425624f
                </div>
            </div>


            <div class="row">
<<<<<<< HEAD
			Coming soon...
               <?php /*?> <div class="col-lg-6">
=======
                <div class="col-lg-6">
>>>>>>> 05deb3a9c5e736f16e29227a8ea227401425624f
                    <div class="card-box">
                        <div class="dropdown pull-right">
                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>

                        <h4 class="header-title m-t-0 m-b-30">Basic Form</h4>

                        <form action="#" data-parsley-validate novalidate>
                            <div class="form-group">
                                <label for="userName">User Name*</label>
                                <input type="text" name="nick" parsley-trigger="change" required
                                       placeholder="Enter user name" class="form-control" id="userName">
                            </div>
                            <div class="form-group">
                                <label for="emailAddress">Email address*</label>
                                <input type="email" name="email" parsley-trigger="change" required
                                       placeholder="Enter email" class="form-control" id="emailAddress">
                            </div>
                            <div class="form-group">
                                <label for="pass1">Password*</label>
                                <input id="pass1" type="password" placeholder="Password" required
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="passWord2">Confirm Password *</label>
                                <input data-parsley-equalto="#pass1" type="password" required
                                       placeholder="Password" class="form-control" id="passWord2">
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="remember-1" type="checkbox">
                                    <label for="remember-1"> Remember me </label>
                                </div>
                            </div>

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    Submit
                                </button>
                                <button type="reset" class="btn btn-link waves-effect m-l-5">
                                    Cancel
                                </button>
                            </div>

                        </form>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="dropdown pull-right">
                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>

                        <h4 class="header-title m-t-0 m-b-30">Horizontal Form</h4>

                        <form class="form-horizontal" role="form" data-parsley-validate novalidate>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">Email*</label>
                                <div class="col-sm-7">
                                    <input type="email" required parsley-type="email" class="form-control"
                                           id="inputEmail3" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="hori-pass1" class="col-sm-4 control-label">Password*</label>
                                <div class="col-sm-7">
                                    <input id="hori-pass1" type="password" placeholder="Password" required
                                           class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="hori-pass2" class="col-sm-4 control-label">Confirm Password
                                    *</label>
                                <div class="col-sm-7">
                                    <input data-parsley-equalto="#hori-pass1" type="password" required
                                           placeholder="Password" class="form-control" id="hori-pass2">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="webSite" class="col-sm-4 control-label">Web Site*</label>
                                <div class="col-sm-7">
                                    <input type="url" required parsley-type="url" class="form-control"
                                           id="webSite" placeholder="URL">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <input id="remember-2" type="checkbox">
                                        <label for="remember-2"> Remember me </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Registrer
                                    </button>
                                    <button type="reset"
                                            class="btn btn-link waves-effect m-l-5">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
<<<<<<< HEAD
                </div><!-- end col --><?php */?>
=======
                </div><!-- end col -->
>>>>>>> 05deb3a9c5e736f16e29227a8ea227401425624f
            </div>
            <!-- end row -->



        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer">

    </footer>

</div>
@endsection