@extends('admin.layout.login')

@section('title')
    <title> Listtee | Web application </title>
@stop

<div class="text-center logo-alt-box">
    <a href="index.html" class="logo"> <span><img src="{{ URL('assets/admin/images/logo.png') }}" alt="logo" style="height: 50px;" ></span></a>

</div>
@section('content')
    <div class="wrapper-page">

        <div class="m-t-30 card-box">
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">Register</h4>
            </div>
                <div class="panel-body">
                    <form class="form-horizontal m-t-10" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <div class="col-xs-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-xs-12">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                            <div class="col-xs-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="checkbox checkbox-custom">
                                    <input id="checkbox-signup" type="checkbox" checked="checked">
                                    <label for="checkbox-signup">I accept <a href="#">Terms and Conditions</a></label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light text-uppercase" type="submit">
                                    <i class="fa fa-btn fa-user"></i>  Register
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
        </div>
        <!-- end card-box -->
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="text-muted">Already have account?<a href="{{ url('/login') }}" class="text-primary m-l-5"><b>Sign In</b></a></p>
            </div>
        </div>
    </div>
    <!-- end wrapper page -->
@endsection
