
<?php

$url = "https://www.tripadvisor.in/Hotels-g187147-Paris_Ile_de_France-Hotels.html";
$status = "tripadvisor";


$value = getReviewerName($url,$status);
echo "<pre>";
print_r($value);
echo "</pre>";


function getContents($url, $status = NULL){
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $html = curl_exec($ch);
    //curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    # Create a DOM parser object
    $dom = new DOMDocument();

    # Parse the HTML from Google.
    # The @ before the method call suppresses any warnings that
    # loadHTML might throw because of invalid HTML in the page.
    return @$dom->loadHTML($html);
}


/////////////////////// function to get reviews text //////////////////////////////
function getReviewText($url, $status = NULL){

    $content = getContents($url,$status);

    $i = 1;
    # Iterate over all the <a> tags
    foreach ($dom->getElementsByTagName('p') as $content) {
        # Show the <a href>
        //echo "@#"."<pre>"; print_r($content);echo "</pre>";

        // For yelp count
        // yellowpages.com
        if ($status == 'yelp' ||  $status == 'citySearch') {
            if ($content->getAttribute('itemprop') == 'description') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }
        if($status == 'judysbook' || $status == 'kudzu' || $status == 'ukYahoo' || $status == 'merchantcircle') {
            if ($content->getAttribute('itemprop') == 'reviewBody') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }

        if($status == 'local' || $status == 'insiderpages') {
            if ($content->getAttribute('class') == 'description') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }

        if($status == 'tripadvisor') {
            if ($content->getAttribute('class') == 'partial_entry') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }

        if($status == 'superpages') {
            //echo "@#"."<pre>"; print_r($content->getAttribute('class'));echo "</pre>";
            if ($content->getAttribute('class') == 'ng-binding') {
                echo "@#"."<pre>"; print_r($content);echo "</pre>";
            }
        }
    }


    foreach ($dom->getElementsByTagName('div') as $content) {
        if($status == 'yellowpages'){
            if ($content->getAttribute('itemprop') == 'reviewBody') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }
    }

    foreach ($dom->getElementsByTagName('span') as $content) {
        if($status == 'dexknows'){
            if ($content->getAttribute('class') == 'description') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }
    }

}


/////////////////////// function to get reviews rating //////////////////////////////
function getReviewRating($url, $status = NULL){

    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $html = curl_exec($ch);
    //curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    # Create a DOM parser object
    $dom = new DOMDocument();

    # Parse the HTML from Google.
    # The @ before the method call suppresses any warnings that
    # loadHTML might throw because of invalid HTML in the page.
    @$dom->loadHTML($html);

    $i = 1;
    # Iterate over all the <a> tags
    foreach ($dom->getElementsByTagName('div') as $content) {
        # Show the <a href>
        //echo "@#"."<pre>"; print_r($content);echo "</pre>";

        // For yelp count
        // yellowpages.com
        if ($status == 'yelp' ||  $status == 'citySearch') {
            if ($content->getAttribute('itemprop') == 'reviewRating') {
                echo "@#"."<pre>"; print_r($content);echo "</pre>";
            }
        }
        if($status == 'judysbook' || $status == 'kudzu' || $status == 'ukYahoo' || $status == 'merchantcircle') {
            if ($content->getAttribute('itemprop') == 'reviewBody') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }

        if($status == 'local' || $status == 'insiderpages') {
            if ($content->getAttribute('class') == 'description') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }

        if($status == 'tripadvisor') {

            if ($content->getAttribute('class') == 'partial_entry') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }

        if($status == 'superpages') {
            //echo "@#"."<pre>"; print_r($content->getAttribute('class'));echo "</pre>";
            if ($content->getAttribute('class') == 'ng-binding') {
                echo "@#"."<pre>"; print_r($content);echo "</pre>";
            }
        }
    }


    foreach ($dom->getElementsByTagName('div') as $content) {
        if($status == 'yellowpages'){
            if ($content->getAttribute('itemprop') == 'reviewBody') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }
    }

    foreach ($dom->getElementsByTagName('span') as $content) {
        if($status == 'dexknows'){
            if ($content->getAttribute('class') == 'description') {
                echo "@#"."<pre>"; print_r($content->textContent);echo "</pre>";
            }
        }
    }

}



/////////////////////// function to get reviewer name //////////////////////////////
function getReviewerName($url, $status = NULL){

    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $html = curl_exec($ch);
    //curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    # Create a DOM parser object
    $dom = new DOMDocument();

    # Parse the HTML from Google.
    # The @ before the method call suppresses any warnings that
    # loadHTML might throw because of invalid HTML in the page.
    @$dom->loadHTML($html);

    $i = 1;
    # Iterate over all the <a> tags
   /* foreach ($dom->getElementsByTagName('a') as $content) {
        # Show the <a href>
        //echo "@#"."<pre>"; print_r($content);echo "</pre>";

        // For yelp count
        // yellowpages.com
        if ($status == 'yelp') {
            if ($content->getAttribute('class') == 'user-display-name') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }

        if($status == 'insiderpages') {
            if ($content->getAttribute('class') == 'reviewer') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }

        if($status == 'dexknows'){
            if ($content->getAttribute('class') == 'business-name') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }
    }


    foreach ($dom->getElementsByTagName('div') as $content) {
        if($status == 'yellowpages'){
            if ($content->getAttribute('class') == 'author') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }
    }

    foreach ($dom->getElementsByTagName('strong') as $content) {
        if($status == 'ukYahoo'){
            if ($content->getAttribute('itemprop') == 'author') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }
    }

    foreach ($dom->getElementsByTagName('b') as $content) {
        if($status == 'merchantcircle'){
            if ($content->getAttribute('itemprop') == 'author') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }
    }*/

    foreach ($dom->getElementsByTagName('span') as $content) {

        echo $content->getAttribute('class');
    }
    /*foreach ($dom->getElementsByTagName('span') as $content) {
        if($status == 'citysearch' || $status == 'kudzu'){
            if ($content->getAttribute('itemprop') == 'author') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }
        if($status == 'local'){
            if ($content->getAttribute('itemprop') == 'name') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }
        if($status == 'tripadvisor') {
            echo "@#"."<pre>"; print_r($content);echo "</pre>";
            if ($content->getAttribute('class') == 'scrname') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }
        if($status == 'superpages') {
            //echo "@#"."<pre>"; print_r($content->getAttribute('class'));echo "</pre>";
            if ($content->getAttribute('class') == 'ng-binding') {
                echo "@#"."<pre>"; print_r($content);echo "</pre>";
            }
        }
    }

    foreach ($dom->getElementsByTagName('h1') as $content) {        if($status == 'judysbook'){
            if ($content->getAttribute('itemprop') == 'name') {
                echo "@#"."<pre>"; print_r($content->nodeValue);echo "</pre>";
            }
        }
    }*/

}

?>