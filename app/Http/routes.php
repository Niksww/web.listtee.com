<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Admin\AdminController@dashboard');

Route::get('/admin/dashboard', [
    'as' => 'dashboard', 'uses' => 'Admin\AdminController@dashboard'
]);

Route::auth();


Route::group(array('middleware' => 'auth'), function () {
    //Login Routes...
    Route::get('/employee/login',[
    'as'=> 'employee.login', 'uses'=>'EmployeeAuth\AuthController@showLoginForm']);
    Route::post('/employee/login','EmployeeAuth\AuthController@login');
    Route::get('/employee/logout',[ 'as'=>'employee.logout', 'uses'=>'EmployeeAuth\AuthController@logout']);

    // Registration Routes...
    Route::get('employee/register', 'EmployeeAuth\AuthController@showRegistrationForm');
    Route::post('employee/register', 'EmployeeAuth\AuthController@register');

    Route::post('employee/password/email','EmployeeAuth\PasswordController@sendResetLinkEmail');
    Route::post('employee/password/reset','EmployeeAuth\PasswordController@reset');
    Route::get('employee/password/reset/{token?}','EmployeeAuth\PasswordController@showResetForm');

    Route::get('/employee', 'EmployeeController@index');
});

Route::get('/employee/dashboard', [
    'as' => 'employee.dashboard', 'uses' => 'EmployeeController@dashboard'
]);



/*Route::group(array('middleware' => 'auth'), function() {

    Route::get('/admin/dashboard', ['as' => 'dashboard', 'uses' => 'Admin\AdminController@dashboard']);

});*/
Route::get('/admin', [
    'as' => 'admin', 'uses' => 'Admin\AdminController@index'
]);
Route::get('/admin/additem', [
    'as' => 'addItem', 'uses' => 'Admin\AdminController@addItem'
]);

Route::resource('admin/groups', 'Admin\Groups\GroupsController');

Route::resource('admin/product', 'Admin\ProductController');


Route::get('getImport', 'Admin\ExcelController@getImport');
Route::post('admin/postImport', 'Admin\ExcelController@postImport');



