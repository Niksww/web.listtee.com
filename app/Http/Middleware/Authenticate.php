<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    //protected $guard = 'user';

    public function handle($request, Closure $next, $guard = null)
    {
        /*echo $guard;
       // echo $this->getMiddleware();
        echo Auth::guard($guard)->user();
        echo __LINE__."<br/>";
        echo Auth::guard($guard)->guest();*/

      /*  if (Auth::guard($guard)->user() == 'auth') {

            echo Auth::guard($guard)->user();
        }

      if (Auth::guard($guard)->user() == 'employee') {

            echo Auth::guard($guard)->user();
        }*/


        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
               //echo Auth::guard($guard)->user();
               //echo Auth::guard()->user();
                //return redirect()->guest('login');
               return $next($request);
            }
        }

        return $next($request);
    }
}
