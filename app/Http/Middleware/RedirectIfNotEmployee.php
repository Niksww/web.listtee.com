<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotEmployee
{

    protected $guard = 'employee';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    public function handle($request, Closure $next, $guard = null)
    {
        
        if (Auth::guard($this->guard)->check())
        {
            return view('employee.dashboard');
        }



       // return $next($request);
       return redirect('employee/login');
    }
}