<?php

namespace App\Http\Controllers\EmployeeAuth;

use App\Employee;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/employee/dashboard';

    protected $redirectAfterLogout = '/employee/login';

    protected $guard = 'employee';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       /* echo __LINE__.'<br/>';
        echo $this->getGuard();*/
        //print_r($this->getMiddleware());
        /*if($this->getMiddleware() == $this->guard){

            echo "here";
            exit;
        }*/
        $this->middleware($this->getMiddleware(), ['except' => 'employee.logout']);

    }

    public function showLoginForm(){

        if($this->getMiddleware() == $this->guard){

           /* echo "here";
            exit;*/
        }
        if(view()->exists('auth.authenticate')){

            return view('auth.authenticate');
        }
       
        return view('employee.auth.login');


    }

    public function showRegistrationForm(){

        return view('employee.auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:employees,email',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Employee::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


}
