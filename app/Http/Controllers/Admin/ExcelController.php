<?php

namespace App\Http\Controllers\Admin;


use App\Product;
use Input;
use DB;
use Excel;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExcelController extends Controller
{

    public $getProductCountBefore = null;

    public $getProductAfterImport = null;

    public function __construct()
    {
        $this->getProductCountBefore = Product::count();
    }

    public function getImport(){
        return view('admin.product.importProduct');
    }

    public function getProductCountAfter($beforeCount){

        $this->getProductAfterImport =  Product::count() - $this->getProductCountBefore;
        if($this->getProductAfterImport > 0){
           $message = $this->getProductAfterImport . ' Product(s) Imported Successfully';
        }else{
            $message = "$this->getProductAfterImport Product Imported";
        }

        return $message;
    }

    public function postImport(Request $request){

        Excel::load($request->file('products'), function($reader){

               $reader->each(function($sheet){
                  Product::firstOrCreate($sheet->toArray());


            });

            Session::flash('flash_message', $this->getProductCountAfter($this->getProductCountBefore));
        });

        return redirect()->route('admin.product.index');

    }
}
