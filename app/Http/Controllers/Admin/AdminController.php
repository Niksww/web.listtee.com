<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    protected $layout = 'layout.master';

    public function __construct()
    {
        $this->middleware('auth:user');
    }
    public function index(){

        return view('admin.dashboard');
    }
    
    public function dashboard(){

        return view('admin.dashboard');
    }

    public function addItem(){

        return view('admin.addItem');

    }
    

}

?>
