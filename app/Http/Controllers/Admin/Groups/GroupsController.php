<?php

namespace App\Http\Controllers\Admin\Groups;

use App\productGroups;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }
    //
    public function index(Request $request){
       // return view('admin.groups');
        $groups = productGroups::all();

        //return view('admin.groups')->withGroups($groups);


        if($request->get('search')){
            $groups = productGroups::where("title", "LIKE", "%{$request->get('search')}%")
                ->paginate(5);
        }else{
            $groups = productGroups::paginate(10);
        }
        //return response($groups);
        return view('admin.groups.index', ['groups' => $groups]);

    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|unique:name',
            'description' => 'required',
            'url_key' => 'required|unique:url_key'
        ]);
    }


    protected function create()
    {

            return view('admin.groups.create');
        //return view('admin.groups');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:product_groups',
            'description' => 'required'
        ]);
        $input = $request->all();

        productGroups::create($input);

        Session::flash('flash_message', $request->name. ' Group successfully added!');

        return redirect()->back();

    }

    public function show($id)
    {
        $group = productGroups::findOrFail($id);

        // show the view and pass the product to it
        return view('admin.groups.show',
            [
                'group' => $group,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $group = productGroups::findOrFail($id);
        // show the view and pass the product to it
        return view('admin.groups.edit',
            [
                'group' => $group,

            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        // validate

        $group = productGroups::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        $input = $request->all();

        $group->fill($input)->save();

        Session::flash('flash_message', $request->name. ' successfully updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $group = productGroups::findOrFail($id);
        $groupName = $group->name;
        $group->delete();

        Session::flash('flash_message', $groupName. ' successfully deleted!');

        return redirect()->route('admin.groups.index');
    }
}
