<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\User;
use App\productGroups;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }
    //
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        if (Auth::check())
        {
           echo  $id = Auth::user()->getId();
        }
        echo  Auth::user();
        // get all the products
        $products = Product::where('user_id',$id);

        $session_id = null;
        // load the view and pass the product
        return view('admin.product.index', ['products' => $products, 'sess' => $session_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $groups = productGroups::all()->where('is_enable','Yes');
        return view('admin.product.create', ['groups' => $groups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|unique:products',
            'product_groups_id' => 'required',
            'price' => 'required',
        ]);
        $input = $request->all();

        Product::create($input);

        Session::flash('flash_message', $request->input('name').' successfully added!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $groups = productGroups::all();
        // show the view and pass the product to it
        return view('admin.product.show',
            [
                'product' => $product,
                'groups' => $groups,

            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $product = Product::findOrFail($id);
        $groups = productGroups::all()->where('is_enable','Yes');

        // show the view and pass the product to it
        return view('admin.product.edit',
            [
                'product' => $product,
                'groups' => $groups,

            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        // validate

        $product = Product::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'product_groups_id' => 'required',
            'price' => 'required',
        ]);

        $input = $request->all();

        $product->fill($input)->save();

        Session::flash('flash_message', $request->name. ' successfully updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $productName = $product->name;
        $product->delete();

        Session::flash('flash_message', $productName. ' successfully deleted!');

        return redirect()->route('admin.product.index');
    }
}
