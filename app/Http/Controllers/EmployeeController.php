<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{


    public function __construct(){
        $this->middleware('auth:employee');
    }
    public function index(){
        //return Auth::guard('employee')->user();
        return redirect('/employee/login');
    }

    public function dashboard(){

        //return Auth::guard('employee')->user();
        return view('employee.dashboard');
    }
}
