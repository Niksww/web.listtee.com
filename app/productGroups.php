<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productGroups extends Model
{
    //
    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'url_key',
        'is_enable',
        'user_id',
        'created_by'

    ];
}
