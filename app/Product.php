<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends model
{

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'product_groups_id',
        'url',
        'asin',
        'sku',
        'price',
        'quantity',
        'user_id',
        'created_by'


    ];
}
